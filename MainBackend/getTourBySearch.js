// export const getTourBySearch = async (req, res) => {
//     // here 'i' means case sensitive
//     const city = new RegExp(req.query.city, "i");
//     const distance = parseInt(req.query.distance);
//     const maxGroupSize = parseInt(req.query.maxGroupSize);
//     try {
//         // gte means greater than equal
//         const tours = await Tour.find({
//             city,
//             distance: { $gte: distance },
//             maxGroupSize: { $gte: maxGroupSize },
//         });

//         res.status(200).json({
//             success: true,
//             message: "successful",
//             data: tours,
//         });
//     } catch (err) {
//         res.status(404).json({
//             success: false,
//             message: "not found",
//         })
//     }
// }



const express = require('express');
const router = express.Router();
const mongodb = require('mongodb').MongoClient;

router.get('/', (req, res) => {
    const city = new RegExp(req.query.city, "i");
    const distance = parseInt(req.query.distance);
    const maxGroupSize = parseInt(req.query.maxGroupSize);

    mongodb.connect('mongodb://localhost:27017/tour_travel', (err, client) => {
        if (err) {
            console.error("mongodb connection error", err);
            return res.status(500).send("Server error");
        }

        const db = client.db('tour_travel');
        db.collection('tourdetails').find({
            city,
            distance: { $gte: distance },
            maxGroupSize: { $gte: maxGroupSize },
        }).toArray((err, data) => {
            if (err) {
                console.error("Error fetching data from collection", err);
                return res.status(500).send("Error fetching data");
            }

            if (data.length > 0) {
                res.status(200).json({
                    success: true,
                    message: "successful",
                    data: data,
                });
            } else {
                res.status(404).send("No tours found");
            }
        });
    });
});

module.exports = router;
