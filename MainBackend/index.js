const express = require('express');
const fs = require('fs')
const app = express();
const PORT = 5000;
const cors = require("cors");


app.use(cors());
app.use(express.json());
app.use('/insert',require('./insert'))
app.use('/fetch',require('./fetch'))
app.use('/update',require('./update'))
app.use('/delete', require('./delete'))
app.use('/search', require('./getTourBySearch'));
app.use('/search/getFeaturedTour',require('./getFeaturedTours'))
app.use('/register',require('./register'))
app.use('/login',require('./login'))

app.listen(PORT,()=>{
    console.log(`server is running ${PORT}`)
})