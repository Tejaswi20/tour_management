const express = require('express')
const router =express.Router();
const mongodb = require('mongodb').MongoClient;
module.exports = router.put('/:id',(req, res) => {
    const dataid = req.params.id;
    const data = req.body;

    mongodb.connect('mongodb://localhost:27017/tour_travel',(err, db) => {
        if(err) {
            console.error("error connecting to mongodb", err);
            return res.status(500).send("Internal Server Error");
        }
        db.collection('tourdetails').update({ id: dataid},{ $set: data},(err, result) => {
            if(err){
                console.error("Error updating data in collection", err);
                return res.status(500).send("Internal Server Error");
            }
            else{
                if (result.length === 0){
                    res.status(404).send("data not found")
                }
                else{
                    res.status(200).send("data updated")
                }
            }
        })
    })
})