const express = require('express')
const router =express.Router();
const mongodb = require('mongodb').MongoClient;

module.exports = router.post('/', (req,res) => {
    const data = {
        
        "mail":(req.body.mail),
        "password":(req.body.password)
    }

    mongodb.connect('mongodb://localhost:27017/tour_travel',(err,db) =>{
        if(err){
            console.error("mongodb connection error")
            return res.status(404).send("server is error")  
        }
        else{
            db.collection('tourdetails').insertOne( data, (err, result) =>{
                if(err){
                    console.error("collection error")
                }
                else{
                    res.status(201).send("successfully inserted")
                }
            })
        }
    })
})