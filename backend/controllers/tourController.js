// import Tour from '../models/Tour.js'

// //create new tour

// export const createTour = async (req,res)=>{

//     const newTour = new Tour(req.body)

//     try{
//         const savedTour = await newTour.save()

//         res
//         .status(200)
//         .json({
//             success:true,
//             message:'Successsfully created',
//         data:savedTour
//     })
//     }catch (err){
//         res.
//         status(500)
//         .json({success:false,
//             message:"failed to create.Try again" });

//     }
// };

import Tour from "../models/Tour.js";

// create new tour
export const createTour = async (req, res) => {
  const newTour = new Tour(req.body);

  try {
    const savedTour = await newTour.save();
    res.status(201).json({
      success: true,
      message: "Successfully created",
      data: savedTour,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Failed to create. Try again",
      error: err.message, // Include the error message for debugging
    });
  }
};

// update tour
export const updateTour = async (req, res) => {
  const id = req.params.id;
  try {
    const updateTour = await Tour.findByIdAndUpdate(
      id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json({
      success: true,
      message: "Successfully updated",
      data: updateTour,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "failed to created",
    });
  }
};

// delete tour
export const deleteTour = async (req, res) => {
  const id = req.params.id;
  try {
    await Tour.findByIdAndDelete(
      id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json({
      success: true,
      message: "Successfully deleted",
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "failed to delete",
    });
  }
};

// getSingle tour
export const getSingleTour = async (req, res) => {
  const id = req.params.id;
  try {
    const tour = await Tour.findById(id);

    res.status(200).json({
      success: true,
      message: "Successfully ",
      data: tour,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "not found",
    });
  }
};

// getAll tour
export const getAllTour = async (req, res) => {
  //for pagination
  const page = parseInt(req.query.page);
  console.log(page);

  try {
    const tours = await Tour.find({})
      .skip(page * 8)
      .limit(8);
    res.status(200).json({
      success: true,
      count: tours.length,
      message: "Successfully retrieved all tours",
      data: tours,
    });
  } catch (err) {
    console.error("Error retrieving tours:", err); // Log the error for debugging purposes
    res.status(404).json({
      success: false,
      message: "Tours not found",
    });
  }
};
