// import express, { response } from 'express'
// import dotenv from 'dotenv'
// import mongoose from 'mongoose'
// import cors from 'cors'
// import cookieParser from 'cookie-parser'
// dotenv.config()
// const app=express()
// const port=process.env.PORT || 8000



// // database comnnection
// const connect = async () => {
//     try {
//         await mongoose.connect(process.env.MONGO_URI,{
//             useNewUrlParser:true,
//             useUnifiedTopology:true
//         })

//         console.log('Mongodb database connected');
//     }catch(err){
//          console.log('Mongodb database connection failed')
//     }
// }
// //for testing
// // app.get('/',(req,res)=>{
// //     res.send("api is working");
// // })



// //middleware
// app.use(express.json());
// app.use(cors());
// app.use(cookieParser());

// app.listen(port,()=>{
//     connect();
//     console.log('server listening on port',port);
// })




// import express from 'express'
// import dotenv from 'dotenv'
// import mongoose from 'mongoose'
// import cors from 'cors'
// import cookieParser from 'cookie-parser'

// import tourRoute from './routes/tours.js'

// dotenv.config()
// const app=express()
// const port=process.env.PORT || 8000

// // database comnnection
// mongoose.set("strictQuery",false);
// const connect=async()=>{
//     try{
//   await mongoose.connect(process.env.MONGO_URI,{
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//   });
//   console.log("MongoDB database connected");
//     }catch(err){
// console.log("MongoDB database connection failed");
//     }
// };
// //for testing
// // app.get('/',(req,res)=>{
// //     res.send("api is working");
// // })
// //middleware
// app.use(express.json());
// app.use(cors());
// app.use(cookieParser());
// app.use("/tours", tourRoute);

// app.listen(port,()=>{
//   connect();
  
//     console.log("server listening on port",port);
// })


import express from 'express';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import cors from 'cors';
import cookieParser from 'cookie-parser';

import tourRoute from './routes/tours.js';

dotenv.config();
const app = express();
const port = process.env.PORT || 8000;




// Database connection
mongoose.set("strictQuery", false);
const connect = async () => {
    try {
        await mongoose.connect(process.env.MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log("MongoDB database connected");
    } catch (err) {
        console.log("MongoDB database connection failed");
        process.exit(1); // Exit the process with a failure
    }
};

// Middleware
app.use(express.json());
app.use(cors());
app.use(cookieParser());

// Routes
app.use("/tours", tourRoute);

// Test route (optional for debugging)
// app.post('/test', (req, res) => {
//     res.status(200).json({
//         message: "Data received",
//         data: req.body
//     });
// });

app.listen(port, () => {
    connect();
    console.log(`Server listening on port ${port}`);
});