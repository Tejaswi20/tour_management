import React from 'react'
import galleryImages from './Galleryimages'
import Masonry, { ResponsiveMasonry} from 'react-responsive-masonry'


const MasonryImagesGallery = () => {
  return (
    
    <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 768: 3, 992: 4 }}>
      <Masonry gutter="1rem">
        {galleryImages.map((item, index) => (
          <img className='masonry__img'
            key={index}
            src={item}
            style={{ width: '100%', display: 'block' , borderRadius: "10px" }}
            alt=""
          />
        ))}
      </Masonry>
    </ResponsiveMasonry>
  );
};



export default MasonryImagesGallery