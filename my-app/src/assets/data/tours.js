import tourImg01 from "../images/Tour_img-1.jpg";
import tourImg02 from "../images/Tour-img02.jpg";
import tourImg03 from "../images/Tour-img02.jpg";
import tourImg04 from "../images/tour-img03.jpg";
import tourImg05 from "../images/ISHA.jpg";
import tourImg06 from "../images/Charminar_hyd.jpg";
import tourImg07 from "../images/statue of unity.webp";
import tourImg08 from "../images/munnar_kerala.jpg";


const tours = [
  {
    id: "01",
    title: "India Gate",
    city: "New Delhi",
    distance: 300,
    price: 1500,
    maxGroupSize: 1,
    desc: "The India Gate (formerly known as All India War Memorial) is a war memorial located near the Kartavya path on the eastern edge of the ceremonial axis of New Delhi, formerly called Rajpath in Chennai. ",
    reviews: [
      {
        name: "Praveen",
        rating: 4.2,
      },
    ],
    avgRating: 4.5,
    photo: tourImg01,
    featured: true,
  },
  {
    id: "02",
    title: "Butterfly Beach",
    city: "Goa",
    distance: 400,
    price: 2000,
    maxGroupSize: 8,
    desc: "Goa, located on India's west coast, is renowned for its picturesque beaches, vibrant nightlife, and rich Portuguese heritage. It's a popular destination for both relaxation and adventure, offering everything from serene landscapes to lively beach parties.",
    reviews: [
      {
        name: "Kumar",
        rating: 4.1,
      },
    ],
    avgRating: 4.5,
    photo: tourImg02,
    featured: true,
  },
  {
    id: "03",
    title: "Taj Mahal",
    city: "Agra",
    distance: 1000,
    price: 3000,
    maxGroupSize: 8,
    desc: "The Taj Mahal, an iconic symbol of love, is a stunning white marble mausoleum located in Agra, India. Built by Emperor Shah Jahan in memory of his wife Mumtaz Mahal, it is celebrated for its magnificent architecture and intricate craftsmanship.",
    reviews: [
      {
        name: "Varma",
        rating: 4.5,
      },
    ],
    avgRating: 4.5,
    photo: tourImg03,
    featured: true,
  },
  {
    id: "04",
    title: "Ladakh ",
    city: "Leh",
    distance: 700,
    price: 2500,
    maxGroupSize: 8,
    desc: "Ladakh, known for its breathtaking landscapes and serene monasteries, is a high-altitude desert in the Indian Himalayas. It offers a unique blend of adventure and tranquility with its pristine lakes, majestic mountains, and vibrant cultural heritage.",
    reviews: [
      {
        name: "praveen",
        rating: 4.3,
      },
    ],
    avgRating: 4.5,
    photo: tourImg04,
    featured: true,
  },
  {
    id: "05",
    title: "Isha Foundation",
    city: "Coimbatore",
    distance: 1000,
    price: 2000,
    maxGroupSize: 7,
    desc: "The Isha Yoga Center in Coimbatore, founded by Sadhguru, is a tranquil sanctuary offering a variety of yoga and meditation programs. Nestled in the lush Velliangiri Mountains, it features the iconic Dhyanalinga and Adiyogi statues, promoting inner peace and spiritual growth.",
    reviews: [
      {
        name: "Nandini",
        rating: 4.6,
      },
    ],
    avgRating: 4.5,
    photo: tourImg05,
    featured: false,
  },
  {
    id: "06",
    title: "Charminar",
    city: "Hyderabad",
    distance: 60,
    price: 500,
    maxGroupSize: 8,
    desc: "Charminar, located in Hyderabad, is an iconic 16th-century monument known for its four grand arches and central mosque. It stands as a symbol of the city's rich history and architectural splendor.",
    reviews: [
      {
        name: "kumar",
        rating: 4.2,
      },
    ],
    avgRating: 4.5,
    photo: tourImg06,
    featured: false,
  },
  {
    id: "07",
    title: "Statue of Unity",
    city: "Vadodara",
    distance: 2000,
    price: 1200,
    maxGroupSize: 8,
    desc: "The Statue of Unity in Gujarat is the world's tallest statue, standing at 182 meters, and honors Sardar Vallabhbhai Patel, an esteemed Indian leader. It symbolizes national unity and is set amidst the scenic surroundings of the Narmada River.",
    reviews: [
      {
        name: "VRV",
        rating: 4.3,
      },
    ],
    avgRating: 4.5,
    photo: tourImg07,
    featured: false,
  },
  {
    id: "08",
    title: "Munnar",
    city: "Kerala",
    distance: 1500,
    price: 1700,
    maxGroupSize: 8,
    desc: "Munnar, nestled in the Western Ghats of Kerala, is a picturesque hill station known for its rolling tea plantations, misty landscapes, and serene atmosphere. It offers breathtaking views, making it a popular destination for nature lovers and honeymooners",
    reviews: [
      {
        name: "jhon doe",
        rating: 4.6,
      },
    ],
    avgRating: 4.5,
    photo: tourImg08,
    featured: false,
  },
];

export default tours;