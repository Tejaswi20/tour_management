import React ,{useState} from "react"
import { Container, Row, Col, Form, FormGroup, Button } from 'reactstrap';

import { Link } from 'react-router-dom'

import '../styles/login.css'

import loginImg from '../assets/images/login.jpg'
import userIcon from '../assets/images/user.png'

import axios from 'axios'

const Login=()=> {

  const [credentials, setCredentials] = useState({
   email:'',
   password:''
  
  })

  // const handleClick = e =>{
  //   e.preventDefault()
  // };


  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:5000/login', {
    
        mail: credentials.email,
        password: credentials.password
      });
      console.log(response.data);
      // Handle success response here (e.g., show a success message, redirect to another page)
    } catch (error) {
      console.error("There was an error registering!", error);
      // Handle error response here (e.g., show an error message)
    }
    handleReset();
  };

  const handleReset = ()=>{
    

  setCredentials({
    
    email: '',
    password: ''
  });
}
  const handleChange = e => {
    setCredentials(prev =>({...prev , [e.target.id]:e.target.value}))
   };

  return <section>
    <Container>
      <Row>
        <Col lg='8' className="m-auto">
          <div className="login__container d-flex justify-content-between">
            <div className="login__img">
              <img src={loginImg} alt=""/>
            </div>

            <div className="login__form">
              <div className="user">
                <img src={userIcon} alt="" />
              </div>
              <h2>Login</h2>
              <Form onSubmit={handleSubmit}>
                <FormGroup>
                  <input type="text" placeholder="Email" required id="email" onChange={handleChange} />
                </FormGroup>
                <FormGroup>
                  <input type="password" placeholder="password" required id="password" onChange={handleChange} />
                </FormGroup>
                <Button className="btn secondary__btn auth__btn" type="submit" >Login</Button>
              </Form>
              <p>Don't hava an account? <Link to='/register'>Create</Link></p>
            </div>
          </div>
        </Col>
      </Row>

    </Container>

  </section>

};

export default Login;
