// import React ,{useState} from "react"
// import { Container, Row, Col, Form, FormGroup, Button } from 'reactstrap';

// import { Link } from 'react-router-dom'
// import axios from 'axios';

// import '../styles/login.css'

// import registerImg from '../assets/images/register.png'
// import userIcon from '../assets/images/user.png'

// const Register=()=> {

//   const [credentials, setCredentials] = useState({
//     username:'',
//    email:'',
//    password:''

//   })

//   const handleClick = async e =>{
//     e.preventDefault();
//     try {
//       const response = await axios.post('http://localhost:5000/register', {
//         username: credentials.username,
//         mail: credentials.email,
//         password: credentials.password
//       });
//       console.log(response.data);
//       // Handle success response here (e.g., show a success message, redirect to another page)
//     } catch (error) {
//       console.error("There was an error registering!", error);
//       // Handle error response here (e.g., show an error message)
//     }

//   };

//   const handleChange = e => {
//     setCredentials(prev =>({...prev , [e.target.id]:e.target.value}))
//    };

//   return <section>
//     <Container>
//       <Row>
//         <Col lg='8' className="m-auto">
//           <div className="login__container d-flex justify-content-between">
//             <div className="login__img">
//               <img src={registerImg} alt=""/>
//             </div>

//             <div className="login__form">
//               <div className="user">
//                 <img src={userIcon} alt="" />
//               </div>
//               <h2>Register</h2>
//               <Form onSubmit={handleClick}>
//               <FormGroup>
//                   <input type="text" placeholder="Username" required id="username" onChange={handleChange} />
//               </FormGroup>
//                 <FormGroup>
//                   <input type="text" placeholder="Email" required id="email" onChange={handleChange} />
//                 </FormGroup>
//                 <FormGroup>
//                   <input type="password" placeholder="password" required id="password" onChange={handleChange} />
//                 </FormGroup>
//                 <Button className="btn secondary__btn auth__btn" type="submit" >Create Account</Button>
//               </Form>
//               <p>Already  have an account? <Link to='/login'>Login</Link></p>
//             </div>
//           </div>
//         </Col>
//       </Row>

//     </Container>

//   </section>

// };

// export default Register;




import React, { useState } from "react";
import { Container, Row, Col, Form, FormGroup, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../styles/login.css';

import registerImg from '../assets/images/register.png';
import userIcon from '../assets/images/user.png';

const Register = () => {
  const [credentials, setCredentials] = useState({
    username: '',
    email: '',
    password: ''
  });

  const handleChange = e => {
    setCredentials(prev => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:5000/register', {
        username: credentials.username,
        mail: credentials.email,
        password: credentials.password
      });
      console.log(response.data);
      // Handle success response here (e.g., show a success message, redirect to another page)
    } catch (error) {
      console.error("There was an error registering!", error);
      // Handle error response here (e.g., show an error message)
    }
    handleReset();
  };

  const handleReset = ()=>{
    

  setCredentials({
    username: '',
    email: '',
    password: ''
  });
}

  return (
    <section>
      <Container>
        <Row>
          <Col lg='8' className="m-auto">
            <div className="login__container d-flex justify-content-between">
              <div className="login__img">
                <img src={registerImg} alt="" />
              </div>
              <div className="login__form">
                <div className="user">
                  <img src={userIcon} alt="" />
                </div>
                <h2>Register</h2>
                <Form onSubmit={handleSubmit}>
                  <FormGroup>
                    <input
                      type="text"
                      placeholder="Username"
                      required
                      id="username"
                      onChange={handleChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <input
                      type="text"
                      placeholder="Email"
                      required
                      id="email"
                      onChange={handleChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <input
                      type="password"
                      placeholder="Password"
                      required
                      id="password"
                      onChange={handleChange}
                    />
                  </FormGroup>
                  <Button className="btn secondary__btn auth__btn" type="submit">
                    Create Account
                  </Button>
                </Form>
                <p>Already have an account? <Link to='/login'>Login</Link></p>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Register;

